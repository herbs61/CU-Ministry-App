'use strict';

// modal variables
const modal = document.querySelector('[data-modal]');
const modalCloseBtn = document.querySelector('[data-modal-close]');
const modalCloseOverlay = document.querySelector('[data-modal-overlay]');

// modal function
const modalCloseFunc = function () { modal.classList.add('closed') }

// modal eventListener
modalCloseOverlay.addEventListener('click', modalCloseFunc);
modalCloseBtn.addEventListener('click', modalCloseFunc);





// notification toast variables
const notificationToast = document.querySelector('[data-toast]');
const toastCloseBtn = document.querySelector('[data-toast-close]');

// notification toast eventListener
toastCloseBtn.addEventListener('click', function () {
  notificationToast.classList.add('closed');
});





// mobile menu variables
const mobileMenuOpenBtn = document.querySelectorAll('[data-mobile-menu-open-btn]');
const mobileMenu = document.querySelectorAll('[data-mobile-menu]');
const mobileMenuCloseBtn = document.querySelectorAll('[data-mobile-menu-close-btn]');
const overlay = document.querySelector('[data-overlay]');

for (let i = 0; i < mobileMenuOpenBtn.length; i++) {

  // mobile menu function
  const mobileMenuCloseFunc = function () {
    mobileMenu[i].classList.remove('active');
    overlay.classList.remove('active');
  }

  mobileMenuOpenBtn[i].addEventListener('click', function () {
    mobileMenu[i].classList.add('active');
    overlay.classList.add('active');
  });

  mobileMenuCloseBtn[i].addEventListener('click', mobileMenuCloseFunc);
  overlay.addEventListener('click', mobileMenuCloseFunc);

}





// accordion variables
const accordionBtn = document.querySelectorAll('[data-accordion-btn]');
const accordion = document.querySelectorAll('[data-accordion]');

for (let i = 0; i < accordionBtn.length; i++) {

  accordionBtn[i].addEventListener('click', function () {

    const clickedBtn = this.nextElementSibling.classList.contains('active');

    for (let i = 0; i < accordion.length; i++) {

      if (clickedBtn) break;

      if (accordion[i].classList.contains('active')) {

        accordion[i].classList.remove('active');
        accordionBtn[i].classList.remove('active');

      }

    }

    this.nextElementSibling.classList.toggle('active');
    this.classList.toggle('active');

  });

}

// This works for the prayer request,it sends request to a whatsapp number  
function Whatsapp(){
  var message = document.getElementById("message").value;

  var url = "https://wa.me/+233241348011?text="
  +"*Prayer Request :* "+message+"%0a";

  window.open(url,'_blank').focus();
}

// This works for testimony, it sends testimonies to a whatsapp number
function whatsApp(){
  var Testimony = document.getElementById("name").value;

  var num = "https://wa.me/+233265462859?text="
  +"*Testimony :* "+Testimony+"%0a";

  window.open(num,'_blank').focus();
}

function prayer(){
  var Name = document.getElementById("name").value;
  var Programme = document.getElementById("programme").value;
  var Level = document.getElementById('level').value;
  var Hostel = document.getElementById('hostel').value;
  var phoneNumber = document.getElementById('phone').value;

  var pray = "https://wa.me/+233265462859?text="
  +"*Name :* "+Name+"%0a"
  +"*programme :* "+Programme+"%0a"
  +"*Level :* "+Level+"%0a"
  +"*Hostel:* "+Hostel+"%0a"
  +"*Phone Number:* "+phoneNumber+"%0a";

  window.open(pray,'_blank').focus();
}





