from django.urls import path
from . import views
from django.contrib.auth import views as authentication_views



urlpatterns = [
    path('',views.main,name='main'),
    path('homepage',views.homepage,name='homepage'),
    path('signin/',authentication_views.LoginView.as_view(template_name='user/signin.html'),name='signin'),
    path('signOut/',authentication_views.LogoutView.as_view(template_name='user/SignOut.html'),name='signOut'),
    path('home/',views.home,name='home'),
    path('registration/',views.registration,name='registration'),
    path('ministry/',views.ministry,name='ministry'),
    path('joinMinistry/',views.joinMinistry,name='joinMinistry'),
    # Joining of various ministry
    path('prayerMinistry',views.prayerMinistry,name='prayerMinistry'),
    path('media/',views.media,name='media'),
    path('praiseWorship/',views.praiseWorship,name='praiseWorship'),
    path('mcc/',views.mcc,name='mcc'),
    path('CJC/',views.CJC,name='CJC'),
    path('LG/',views.LG,name='LG'),
    path('communion/',views.communion,name='communion'),
    path('discipleship/',views.discipleship,name='discipleship'),
    path('protocol/',views.protocol,name='protocol'),
    path('missions/',views.missions,name='missions'),
    path('peer/',views.peer,name='peer'),
    path('band/',views.band,name='band'),
    # Url for counselling
    path('counsellor/',views.counsellor,name='counsellor'),
    path('picture/',views.picture,name='picture'), 
    path('station/',views.station,name='station'),  
    path('give/',views.give,name='give'),
    path('chat/',views.chat,name='chat'),
    path('prayer-wall/',views.prayerRequest,name='prayerRequest'),
    path('testimony',views.testimonyTime,name='testimonyTime'),
    
    ]


