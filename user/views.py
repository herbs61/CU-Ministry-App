from django.shortcuts import render, get_object_or_404, reverse
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import SignIn,UserRegistrationForm
from django.contrib.auth import authenticate,login
from .models import preaching
from django.core.paginator import Paginator

# Create your views here.
# def front(request):
#     return render(request,'user/front.html')

# This is for the front page
def main(request):
    return render(request,'user/main.html')
    
def homepage(request):
    return render(request,'user/homepage.html')

# This aspect is to login
def user_login(request):
    if request.method == 'POST':
        form = SignIn(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(request,username=data['username'],password=data['password'])
            if user is not None:
                login(request,user)
                context = {
                    "user":user
                }
                return render(request,'user/home.html', context)
           
            
    else:
        form = SignIn()
    return render(request,'user/login.html',{'form':form})
   
    # It makes sure you are logged before you enter the home  page
@login_required
def home(request):
    return render(request, 'user/home.html')

# This to signOut
def signOut(request):
    return render(request,'user/SignOut.html')

# This is register a new user
def registration(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=True)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return render(request,'user/thanks.html')
       
    else:
        user_form = UserRegistrationForm()
    return render(request,'user/register.html',{'user_form': user_form})

# This for the various ministries
def ministry(request):
    return render(request,'user/ministry.html')
# The various ministries you can join
def prayerMinistry(request):
    return render (request,'user/ministries/prayer.html')

def media(request):
    return render(request,'user/ministries/media.html')

def praiseWorship(request):
    return render(request,'user/ministries/Praise&worship.html')

def mcc(request):
    return render(request,'user/ministries/MCC.html')

def CJC(request):
    return render(request,'user/ministries/CJC.html')

def LG(request):
    return render(request,'user/ministries/LG.html')

def communion(request):
    return render(request,'user/ministries/communion.html')

def discipleship(request):
    return render(request,'user/ministries/discipleship.html')

def protocol(request):
    return render(request,'user/ministries/protocol.html')

def missions(request):
    return render(request,'user/ministries/missions.html')

def peer(request):
    return render(request,'user/ministries/peer.html')

def band(request):
    return render(request,'user/ministries/band.html')


def picture(request):
    # pics= album.objects.all()
    
    # paginator = Paginator(pics,3)
    # page = request.GET.get('page')
    # pics = paginator.get_page(page)
    
    return render(request,'user/gallery.html')

def joinMinistry(request):
    return render(request,'user/join.html')

# This is the counsellors part in the home page
def counsellor(request):
    return render(request,'user/counselling.html') 

#The filling station aspect
def station(request,):
    paginator = Paginator(preaching.objects.all(),19)
    preach = request.GET.get('page')
    audios = paginator.get_page(preach)
    context = {
        'audios': audios
    }
    return render(request,'user/filling-station.html',context)


    
def give(request):
    return render(request,'user/give.html')

def chat(request):
    return render(request,'user/chat.html')

# Prayer request
def prayerRequest(request):
    return render(request,'user/prayer-wall.html')

# Testimony time
def testimonyTime(request):
    return render(request,'user/testimony.html')
    
    
    