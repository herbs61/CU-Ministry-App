from django import forms
from django.contrib.auth.models import User

# For register page for the person to choose what level he's in
level = [tuple([x,x]) for x in range(100,700,100)]
    


# A form to sign in
class SignIn(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    
# A form to register
class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password',widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm password',widget=forms.PasswordInput)
    Level = forms.CharField(label='What level are you in?', 
                            required=True,
                            widget = forms.Select(choices=level))
    
    class Meta:
        model = User
        fields=['username','email','Level']
        
        # This checks whether the password is the same as the confirm password
        def check_password(self):
            if self.cleaned_data['password'] != self.cleaned_data['password2']:
                raise forms.ValidationError('Password do not match')
            return self.cleaned_data['password2']
        
        